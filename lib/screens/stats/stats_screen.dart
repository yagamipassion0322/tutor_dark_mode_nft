import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:styled_widget/styled_widget.dart';

class StatsScreen extends StatefulWidget {
  const StatsScreen({super.key});

  @override
  State<StatsScreen> createState() => _StatsScreenState();
}

class _StatsScreenState extends State<StatsScreen> {
  int selectedMenu = 0;
  List<dynamic> data = [
    {
      'img': 'assets/images/item_1.png',
      'title': 'Azumi',
      'category': 'View info',
      'point': '200055.02',
      'percent': 3.99
    },
    {
      'img': 'assets/images/item_2.png',
      'title': 'Hape prime',
      'category': 'View info',
      'point': '180055.45',
      'percent': 33.79
    },
    {
      'img': 'assets/images/item_3.png',
      'title': 'Cryoto',
      'category': 'View info',
      'point': '90055.62',
      'percent': -6.56
    },
    {
      'img': 'assets/images/item_4.png',
      'title': 'Ape club',
      'category': 'View info',
      'point': '88055.12',
      'percent': 3.99
    },
    {
      'img': 'assets/images/item_5.png',
      'title': 'Bat',
      'category': 'View info',
      'point': '10055.06',
      'percent': 3.99
    },
    {
      'img': 'assets/images/item_6.png',
      'title': 'Mutant',
      'category': 'View info',
      'point': '9095.27',
      'percent': -4.07
    }
  ];

  void setStates() {
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          width: double.infinity,
          child: Text(
            'Stats',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ).padding(bottom: 27),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 80),
          margin: const EdgeInsets.only(bottom: 27),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 1,
                color: const Color.fromARGB(255, 129, 30, 215).withOpacity(.5),
              ),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if (selectedMenu == 1) {
                      selectedMenu = 0;
                    }
                    setStates();
                  },
                  child: Container(
                    padding: EdgeInsets.only(bottom: 14),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 3,
                          color: selectedMenu == 0
                              ? const Color.fromARGB(255, 129, 30, 215)
                              : Colors.transparent,
                        ),
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.bar_chart_rounded,
                          color: selectedMenu == 0
                              ? Colors.white
                              : const Color(0xffEBEBF5).withOpacity(.3),
                          size: 24,
                        ).padding(right: 8),
                        Text(
                          'Ranking',
                          style: TextStyle(
                            fontSize: 16,
                            color: selectedMenu == 0
                                ? Colors.white
                                : const Color(0xffEBEBF5).withOpacity(.3),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if (selectedMenu == 0) {
                      selectedMenu = 1;
                    }
                    setStates();
                  },
                  child: Container(
                    padding: const EdgeInsets.only(bottom: 14),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 3,
                          color: selectedMenu == 1
                              ? const Color.fromARGB(255, 129, 30, 215)
                              : Colors.transparent,
                        ),
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.show_chart_rounded,
                          color: selectedMenu == 1
                              ? Colors.white
                              : const Color(0xffEBEBF5).withOpacity(.3),
                          size: 24,
                        ).padding(right: 8),
                        Text(
                          'Activity',
                          style: TextStyle(
                            fontSize: 16,
                            color: selectedMenu == 1
                                ? Colors.white
                                : const Color(0xffEBEBF5).withOpacity(.3),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 14,
                  vertical: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(27),
                  color: Colors.white.withOpacity(.08),
                  border: Border.all(
                    width: .9,
                    color: const Color.fromARGB(255, 129, 30, 215),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.dashboard,
                      color: const Color(0xffEBEBF5).withOpacity(.3),
                      size: 24,
                    ),
                    Text(
                      'All categories',
                      style: TextStyle(
                        fontSize: 12,
                        color: selectedMenu == 0
                            ? Colors.white
                            : const Color(0xffEBEBF5).withOpacity(.3),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down_sharp,
                      color: const Color(0xffEBEBF5).withOpacity(.3),
                      size: 24,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(width: 25),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 14,
                  vertical: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(27),
                  color: Colors.white.withOpacity(.08),
                  border: Border.all(
                    width: .9,
                    color: const Color.fromARGB(255, 129, 30, 215),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.link,
                      color: const Color(0xffEBEBF5).withOpacity(.3),
                      size: 24,
                    ),
                    Text(
                      'All chains',
                      style: TextStyle(
                        fontSize: 12,
                        color: selectedMenu == 0
                            ? Colors.white
                            : const Color(0xffEBEBF5).withOpacity(.3),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down_sharp,
                      color: const Color(0xffEBEBF5).withOpacity(.3),
                      size: 24,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ).padding(horizontal: 14, bottom: 27),
        Container(
          margin: const EdgeInsets.only(
            left: 14,
            right: 14,
            bottom: 30,
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 18,
            horizontal: 14,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              27,
            ),
            color: Colors.white.withOpacity(.08),
            border: Border.all(
              width: .9,
              color: const Color.fromARGB(255, 129, 30, 215),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate(
                data.length,
                (index) => Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          (index + 1).toString(),
                          style: TextStyle(
                            fontSize: 14,
                            color: const Color(0xffEBEBF5).withOpacity(.6),
                            fontWeight: FontWeight.normal,
                          ),
                        ).padding(right: 10),
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9),
                            image: DecorationImage(
                              image: AssetImage(data[index]['img']),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data[index]['title'] ?? '',
                              style: const TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ).padding(bottom: 8),
                            Text(
                              data[index]['category'] ?? '',
                              style: TextStyle(
                                fontSize: 12,
                                color: const Color(0xffEBEBF5).withOpacity(.6),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ).padding(left: 14),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(
                              'assets/icons/ethereum.svg',
                              colorFilter: ColorFilter.mode(
                                  Colors.white.withOpacity(.7),
                                  BlendMode.srcIn),
                              width: 3,
                              height: 14,
                            ).padding(right: 8),
                            Text(
                              data[index]['point'] ?? '',
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ).padding(bottom: 8),
                        Text(
                          (data[index]['percent'] ?? 0).toString(),
                          style: TextStyle(
                            fontSize: 12,
                            color: (data[index]['percent'] ?? 0) > 0
                                ? const Color(0xff34C759)
                                : const Color(0xffFF453A),
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ],
                ).padding(bottom: 19),
              )
            ],
          ),
        ),
      ],
    );
  }
}
