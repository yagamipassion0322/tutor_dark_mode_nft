import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:nft_app_ui/screens/home/framebase.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.symmetric(
            horizontal: 20, vertical: MediaQuery.of(context).padding.top + 40),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment(0, .75),
            colors: <Color>[
              Color(0xff5C609B),
              Color(0xff787DB3),
              Color(0xff787DB3),
              Color(0xff787DB3),
            ], // Gradient from https://learnui.design/tools/gradient-generator.html
            tileMode: TileMode.mirror,
          ),
          image: DecorationImage(
            image: AssetImage(
              'assets/images/unsplash.png',
            ),
            alignment: Alignment.bottomLeft,
            fit: BoxFit.contain,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Welcome to NFT Marketplace',
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            ClipRRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 27.0, sigmaY: 27.0),
                child: Container(
                  padding: const EdgeInsets.all(27),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: .9,
                      color: Colors.white.withOpacity(.3),
                    ),
                    borderRadius: BorderRadius.circular(27),
                    color: const Color(0xffffffff).withOpacity(.01),
                  ),
                  child: Column(
                    children: [
                      const Text(
                        'Explore d Mint NFTs',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(height: 6),
                      Text(
                        'You can buy and sell the NFTs of the best artists in the world.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: const Color(0xffEBEBF5).withOpacity(.5),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (c) => const FrameBase())),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 12,
                            horizontal: 42,
                          ),
                          margin: const EdgeInsets.only(top: 27),
                          decoration: BoxDecoration(
                            color: const Color(0xff97A9F6).withOpacity(.5),
                            borderRadius: BorderRadius.circular(34),
                            border: Border.all(
                              width: 1.12,
                              color: Colors.white.withOpacity(.5),
                            ),
                          ),
                          child: const Text(
                            'Get start now',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
