import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nft_app_ui/screens/home/home_screen.dart';
import 'package:nft_app_ui/screens/stats/stats_screen.dart';
import 'package:styled_widget/styled_widget.dart';

class FrameBase extends StatefulWidget {
  const FrameBase({super.key});

  @override
  State<FrameBase> createState() => _FrameBaseState();
}

class _FrameBaseState extends State<FrameBase> {
  List<String> menus = [
    'home.svg',
    'chart.svg',
    'plus.svg',
    'search.svg',
    'profile.svg'
  ];
  int currentMenu = 0;

  @override
  void initState() {
    super.initState();
    setStates();
  }

  void setStates() {
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 47, 28, 73),
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: SingleChildScrollView(
          child: currentMenu == 2 ? const StatsScreen() : const HomeScreen(),
        ),
      ),
      bottomNavigationBar: Stack(
        children: [
          Container(
            height: 120,
            padding: const EdgeInsets.only(top: 20),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(45),
                topRight: Radius.circular(45),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 100.0,
                decoration: BoxDecoration(
                  color: const Color(0xff211134),
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0xffffffff).withOpacity(.4),
                      offset: const Offset(0, 0),
                      blurRadius: 40,
                    ),
                  ],
                ),
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ...List.generate(
                        menus.length,
                        (index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                currentMenu = index;
                              });
                            },
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            child: index == 2
                                ? Container(
                                    width: (MediaQuery.of(context).size.width -
                                            30) *
                                        .18,
                                    color: Colors.transparent,
                                  )
                                : Container(
                                    width: (MediaQuery.of(context).size.width -
                                            30) *
                                        .18,
                                    color: Colors.transparent,
                                    padding: const EdgeInsets.only(top: 12),
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          // AnimatedContainer(
                                          //   duration: const Duration(milliseconds: 1500),
                                          //   curve: Curves.fastLinearToSlowEaseIn,
                                          //   width: size.width * .108,
                                          //   height:
                                          //       index == currentMenu ? size.width * .008 : 0,
                                          //   decoration: const BoxDecoration(
                                          //     color: Colors.red,
                                          //     borderRadius: BorderRadius.vertical(
                                          //         bottom: Radius.circular(8)),
                                          //   ),
                                          // ),
                                          // const SizedBox(height: 8),
                                          SvgPicture.asset(
                                            'assets/icons/${menus[index]}',
                                            colorFilter: ColorFilter.mode(
                                                (index == currentMenu
                                                    ? Colors.white
                                                    : Colors.white
                                                        .withOpacity(.7)),
                                                BlendMode.srcIn),
                                            width: 26,
                                            height: 26,
                                          ).padding(bottom: 8.0),
                                        ]),
                                  ),
                          );
                        },
                      )
                    ]),
              ),
            ),
          ),
          Positioned(
            left: size.width * .42,
            child: ClipPath(
              clipper: MyPolygon(),
              child: GestureDetector(
                onTap: () {
                  if (currentMenu != 2) {
                    currentMenu = 2;
                  }
                  setStates();
                },
                child: Container(
                  color: const Color(0xff4A2A71),
                  width: 70,
                  height: 70,
                  child: Center(
                    child: SvgPicture.asset(
                      'assets/icons/plus.svg',
                      colorFilter: ColorFilter.mode(
                          (currentMenu == 2
                              ? Colors.white
                              : Colors.white.withOpacity(.7)),
                          BlendMode.srcIn),
                      width: 26,
                      height: 26,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyPolygon extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.addPolygon([
      Offset(0, size.height / 2),
      Offset(size.width * 1 / 3, size.height),
      Offset(size.width * 2 / 3, size.height),
      Offset(size.width, size.height / 2),
      Offset(size.width * 2 / 3, 0),
      Offset(size.width * 1 / 3, 0)
    ], true);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
