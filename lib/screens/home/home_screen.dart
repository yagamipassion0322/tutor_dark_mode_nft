import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<dynamic> banners = [
    {'img': 'assets/images/banner_1.png', 'title': 'Art 1'},
    {'img': 'assets/images/banner_2.png', 'title': 'Art 2'},
    {'img': 'assets/images/banner_3.png', 'title': 'Art 3'}
  ];
  List<dynamic> collections = [
    {'img': 'assets/images/art_1.png', 'title': '3D Art', 'likes': 100},
    {'img': 'assets/images/art_2.png', 'title': 'Abstract Art', 'likes': 200},
    {'img': 'assets/images/art_3.png', 'title': 'Portrait Art', 'likes': 300}
  ];
  List<dynamic> sellers = [
    {'img': 'assets/images/item_1.png', 'title': '3D Art', 'likes': 100},
    {'img': 'assets/images/item_2.png', 'title': 'Abstract Art', 'likes': 200},
    {'img': 'assets/images/item_3.png', 'title': 'Portrait Art', 'likes': 300}
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          width: double.infinity,
          child: Text(
            'NFT Marketplace',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ).padding(bottom: 27),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(bottom: 27),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                ...List.generate(
                  banners.length,
                  (index) => Container(
                    width: MediaQuery.of(context).size.width * .75,
                    height: 160,
                    padding: const EdgeInsets.only(top: 120),
                    margin: EdgeInsets.only(
                        left: index == 0 ? 20 : 0,
                        right: (index + 1) == banners.length ? 0 : 10),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(banners[index]['img'] ?? ''),
                        fit: BoxFit.cover,
                      ),
                      border: Border.all(
                        width: .9,
                        color: Colors.white.withOpacity(.5),
                      ),
                      borderRadius: BorderRadius.circular(
                        27,
                      ),
                    ),
                    child: Text(
                      banners[index]['title'] ?? '',
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        const Text(
          'Trending collections',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ).padding(bottom: 8, left: 20),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(bottom: 27),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                ...List.generate(
                  collections.length,
                  (index) => Container(
                    width: 150,
                    height: 200,
                    padding: const EdgeInsets.all(9),
                    margin: EdgeInsets.only(
                        left: index == 0 ? 20 : 0,
                        right: (index + 1) == banners.length ? 0 : 9),
                    decoration: BoxDecoration(
                      color: const Color(0xffFFFFFF).withOpacity(.1),
                      border: Border.all(
                        width: .9,
                        color: Colors.white.withOpacity(.3),
                      ),
                      borderRadius: BorderRadius.circular(
                        27,
                      ),
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          height: 140,
                          margin: const EdgeInsets.only(bottom: 9),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image:
                                  AssetImage(collections[index]['img'] ?? ''),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(
                              20,
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              collections[index]['title'] ?? '',
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.favorite,
                                  color: Colors.red,
                                  size: 16,
                                ).padding(right: 4),
                                Text(
                                  (collections[index]['likes'] ?? 0).toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        const Text(
          'Top seller',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ).padding(bottom: 8, left: 20),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(bottom: 27),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                ...List.generate(
                  sellers.length,
                  (index) => Container(
                    width: 150,
                    height: 200,
                    padding: const EdgeInsets.all(9),
                    margin: EdgeInsets.only(
                        left: index == 0 ? 20 : 0,
                        right: (index + 1) == banners.length ? 0 : 9),
                    decoration: BoxDecoration(
                      color: const Color(0xffFFFFFF).withOpacity(.1),
                      border: Border.all(
                        width: .9,
                        color: Colors.white.withOpacity(.3),
                      ),
                      borderRadius: BorderRadius.circular(
                        27,
                      ),
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          height: 140,
                          margin: const EdgeInsets.only(bottom: 9),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(sellers[index]['img'] ?? ''),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(
                              20,
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              sellers[index]['title'] ?? '',
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.favorite,
                                  color: Colors.red,
                                  size: 16,
                                ).padding(right: 4),
                                Text(
                                  (sellers[index]['likes'] ?? 0).toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
